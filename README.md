# Advanced Identity Search and Edit Plugin

This is a full page SailPoint plugin which should allow to advance search and edit the identities.

## Basic Capabilities
- Enhanced Identity Search
- Ability to edit Identity

## Main URL
[GitLab URL](https://sairamvangala@bitbucket.org/sairamvangala/advanced-identity-search.git)