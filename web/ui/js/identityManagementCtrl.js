angular.module('identitySearchPlugin').controller('identityManagementCtrl',function ($scope,editMgmtService,PLUGIN_BASE_URL,REST_BASE_URL) {
    $scope.searchObjects=['Identity','Managed Attribute'];
    $scope.searchResult="";
    $scope.pageSizeList=[5,10,25,50];
    $scope.totalItems="";
    $scope.currentPage=1;
    $scope.pageSize=$scope.pageSizeList[0];
    $scope.searchResult=[];
    $scope.setItemsPerPage = function(num) {
        $scope.itemsPerPage = num;
        $scope.currentPage = 1; //reset to first page
    }
    $scope.searchData=function (searchObject,searchInput) {
        console.log('Before service call');
        $scope.searchResult=[];
        $scope.noOfPages=$scope.searchResult.length/$scope.pageSize;
        console.log("Nof of pages:"+$scope.noOfPages);
            let url=REST_BASE_URL+'/getIdentityNames';
            if(searchInput){
                url=REST_BASE_URL+'/getNamesStartWith/'+searchInput;
            }
            console.error("URL value is :"+url)
            let method ='GET';
            console.log("calling the service for method")
            editMgmtService.getSearchData(url,method).then(function (response) {
                $scope.searchResult=response;
                $scope.totalItems=response.length;
                console.error("total count is:"+$scope.totalItems)
                $scope.pageSize=$scope.searchResult.length
            });
    };
    $scope.setCurrentSelection=function(val){
        console.error("before calling the service:"+val);
        editMgmtService.setIdentitySelection(val);
    }
    $scope.setIdentityApps=function (val){
        console.error("Calling the identity app names"+val);
        editMgmtService.setIdApps(val);
    }
});