var fullPageURL=SailPoint.CONTEXT_PATH+'/plugins/pluginPage.jsf?pn=identitySearchPlugin';
var debugURL=SailPoint.CONTEXT_PATH+'/debug/debug.jsf';

jQuery(document).ready(function () {
    jQuery("ul.navbar-right li:first").before(
        '<li class="dropdown">'+
        '<a href="#" class="dropdown-toggle" role="menuitem" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" id="PreferenceMenu">'+
        '<i role="presentation" aria-hidden="true" class="fa fa-pencil-square-o"></i>'+
        '<span class="sr-only">'+
        'menu. Press ENTER or space to access submenu. To move through items press up or down arrow.'+
        '</span>'+
        '<span role="presentation" aria-hidden="true" class="caret"></span>'+
        '</a>'+
        '<ul class="dropdown-menu" role="menu" aria-expanded="true">'+
        '<li role="presentation">'+
        '<a href="'+fullPageURL+'"  role="menuitem" class="menuitem">'+
        'Advanced Edit'+
        '</a>'+
        '</li>'+
        '<li role="presentation">'+
        '<a href="'+debugURL+'"  role="menuitem" class="menuitem">'+
        'Debug Page'+
        '</a>'+
        '</li>'+
        '</ul>'+
        '</li>'

    );
})