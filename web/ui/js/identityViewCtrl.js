angular.module('identitySearchPlugin').controller('identityViewCtrl', function ($scope, editMgmtService, PLUGIN_BASE_URL, REST_BASE_URL, IIQ_URL) {
    $scope.passedName = editMgmtService.getIdentitySelection();
    $scope.id=editMgmtService.getId();
    $scope.attributes = {};
    let identityUrl = IIQ_URL + '/rest/debug/Identity/';
    $scope.identityResponse = {}
    $scope.getIdentityXml = function () {
        let method = 'GET';
        let updatedUrl = identityUrl +$scope.id;
        console.error("Identity xml url is:" + updatedUrl);
        editMgmtService.getSearchData(updatedUrl, method).then(function (response) {
            $scope.identityResponse = response;
            let returnJson = $scope.identityResponse;
            console.error("Response object:" + $scope.identityResponse.count)
            $scope.dom = $scope.identityResponse.objects[0].xml;
            console.error("before beautify:" + $scope.dom);
        });
    };
    $scope.getAttrbName = function () {
        editMgmtService.getAttributes().then(function (response) {
            $scope.attributes = response;

        });
        console.error("Attributes are:" + $scope.attributes);

    };
});