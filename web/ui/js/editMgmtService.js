angular.module('identitySearchPlugin').service('editMgmtService', function ($http, $q, PLUGIN_BASE_URL, REST_BASE_URL, IIQ_URL) {
    var CsrfToken = PluginHelper.getCsrfToken();

    function doRequest(url, method) {
        console.error("Inside the httpMethod");
        var def = $q.defer();
        $http({
            method: method,
            url: url,
            headers: {
                'X-XSRF-TOKEN': CsrfToken
            }
        }).then(function success(response) {
            var d;
            var data = response.data;
            d = data;
            def.resolve(d);
        }).catch(function onError(error) {
            console.error("Error occured in request method");
            console.error(error);
            def.reject("");
        });
        return def.promise;
    }

    let identityUrl = IIQ_URL + '/rest/debug/Identity/';
    this.getSearchData = function (url, method) {
        console.debug("before the actual call");
        return doRequest(url, method);
    };

    function postData(url, method, body) {
        console.error("Inside the post method with body:" + JSON.stringify(body));
        var def = $q.defer();
        $http({
            method: method,
            url: url,
            headers: {
                'X-XSRF-TOKEN': CsrfToken,
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(body)
        }).then(function success(response) {
            var d;
            var data = response.data;
            d = data;
            def.resolve(d);
        }).catch(function onError(error) {
            console.error("Error occured in request method");
            console.error(error);
            def.reject("");
        });
        return def.promise;
    }

    this.postReq = function (url, method, body) {
        return postData(url, method, body)
    }
    let identitySelected = "";


    let selectedId="";
    function setId () {
        console.error("Starting the set ID method");
        let url = REST_BASE_URL + '/getIdentityAttrbs/' + identitySelected;
        let method = 'GET';
        console.error("before id call for the URL:" + url);
        doRequest(url, method).then(function (response) {
            console.error("getting Id");
            selectedId= response.ID;
        });
    }
    this.setIdentitySelection = function (val) {
        console.error("Got the val as:" + val);
        identitySelected = val;
        console.error("setting id through ID method");
        setId();
    };
    let idApps="";
    this.setIdApps=function (val){
        console.error("Got the val as:"+val)
        identitySelected=val;
        console.error("calling post method to val call");
        let method='POST';
        let url = REST_BASE_URL + '/getAppLinks'+'/'+identitySelected;
        doRequest(url,method).then(function (response){
           console.error("setting response");
           idApps=response;
        });
    };
    this.getId=function (){
        console.error("calling getId function:"+selectedId);
      return selectedId;
    };
    this.getIdentitySelection = function () {
        return identitySelected;
    }
    this.getAttributes = function () {
        let url = REST_BASE_URL + '/getAttributeNames';
        let method = 'GET';
        return doRequest(url, method);
    }
    this.getIdApps=function (){
        console.error("idApps are:"+JSON.stringify(idApps));
        return idApps;
    }
});