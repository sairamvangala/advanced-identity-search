angular.module('identitySearchPlugin').controller('identityRfshCtrl', function ($scope, editMgmtService, PLUGIN_BASE_URL, REST_BASE_URL) {
    $scope.passedName = editMgmtService.getIdentitySelection();
    $scope.attributes = {};
    $scope.getAttrbName = function () {
        editMgmtService.getAttributes().then(function (response) {
            $scope.attributes = response;
        });
        console.error("Attributes are:" + $scope.attributes);
    };
    // TODO - Need to add explicit values to override the existing task def configuration for refresh.
    $scope.generateJson = function () {
        $scope.loading = true;
        var refreshOpt = {
            "filter": "name==\"" + $scope.passedName + "\"",
            "promoteAttributes": "\"" + $scope.promoteAttributes + "\"",
            "refreshIdentityEntitlements": "\"" + $scope.refreshIdentityEntitlements + "\"",
            "refreshManagerStatus": "\"" + $scope.refreshManagerStatus + "\"",
            "correlateEntitlements": "\"" + $scope.correlateEntitlements + "\"",
            "provision": "\"" + $scope.provision + "\"",
            "synchronizeAttributes": "\"" + $scope.synchronizeAttributes + "\"",
            "processTriggers": "\"" + $scope.processTriggers + "\""
        };
        console.log("Got the Json Object as:" + refreshOpt);
        let method = 'POST';
        let url = REST_BASE_URL + '/refreshIdentity';
        editMgmtService.postReq(url, method, refreshOpt).then(function (response) {
            $scope.refreshResp = response;
            console.error("Response is:" + response);
            $scope.loading = false;
        });
    };

});