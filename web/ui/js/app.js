/**
 * Create the module.
 */

var ksModule = angular.module('identitySearchPlugin', ['ngRoute',
    'ngTable',
    'ngAnimate',
    'ngMaterial',
    'ui.bootstrap',
    'ui.bootstrap.popover',
    'ui',
    'sailpoint.dataview'
]);

/**
 * Define any configs or statics
 */
ksModule.config(function ($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = "CSRF-TOKEN";
});
ksModule.constant('PLUGIN_BASE_URL', SailPoint.CONTEXT_PATH + '/plugin/identitySearchPlugin');
ksModule.constant('REST_BASE_URL', SailPoint.CONTEXT_PATH + '/plugin/rest/advancedEdit');
ksModule.constant('IIQ_URL', SailPoint.CONTEXT_PATH);
ksModule.config(function ($routeProvider, PLUGIN_BASE_URL) {
    $routeProvider
        .when('/', {
            controller: 'identityManagementCtrl',
            templateUrl: PLUGIN_BASE_URL + '/ui/view/mainIdentityView.html'
        })
        .when('/editIdentity', {
            controller: 'identityEditCtrl',
            templateUrl: PLUGIN_BASE_URL + '/ui/view/identityEdit.html'
        })
        .when('/showIdentity', {
        controller: 'identityViewCtrl',
        templateUrl: PLUGIN_BASE_URL + '/ui/view/identityView.html'
         }).when('/refreshIdentity', {
        controller: 'identityRfshCtrl',
        templateUrl: PLUGIN_BASE_URL + '/ui/view/identityRefresh.html'
         }).when('/singleActAggrt', {
        controller: 'singleActAggrtCtrl',
        templateUrl: PLUGIN_BASE_URL + '/ui/view/singleAcct.html'
        })
        .otherwise({
            redirectTo: '/'
        });
});
ksModule.config(['$locationProvider', function ($locationProvider) {
    $locationProvider.hashPrefix('');
}]);
ksModule.value('ui.config', {
    codemirror: {
        mode: 'xml',
        lineNumbers: true,
        readOnly: true,
        flattenSpans:true,
        matchBrackets: true
    }
});

