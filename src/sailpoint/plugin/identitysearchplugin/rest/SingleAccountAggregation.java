package sailpoint.plugin.identitysearchplugin.rest;

import org.apache.log4j.Logger;
import sailpoint.api.Aggregator;
import sailpoint.api.SailPointContext;
import sailpoint.api.SailPointFactory;
import sailpoint.connector.Connector;
import sailpoint.connector.ConnectorException;
import sailpoint.object.*;
import sailpoint.tools.GeneralException;

import java.util.HashMap;

public class SingleAccountAggregation {
    public static final Logger log = Logger.getLogger(SingleAccountAggregation.class);

    public String doSingleAggt(String applicationName, String accountName) {

        SailPointContext context = null;
        String errorMessage = "";
        Connector appConnector = null;
        try {
            context = SailPointFactory.getCurrentContext();
            Application appObject = context.getObjectByName(Application.class, applicationName);
            String appConnName = appObject.getConnector();
            log.debug("Application " + applicationName + " uses connector " + appConnName);
            appConnector = sailpoint.connector.ConnectorFactory.getConnector(appObject, null);

            if (null == appConnector) {
                errorMessage = "Failed to construct an instance of connector [" + appConnName + "]";
                return errorMessage;
            }
            log.debug("Connector instantiated, calling getObject() to read account details...");
            ResourceObject rObj = null;
            rObj = appConnector.getObject("account", accountName, null);
            if (null == rObj) {
                errorMessage = "ERROR: Could not get ResourceObject for account: " + accountName;
                log.error(errorMessage);
                return errorMessage;
            }
            log.debug("Got raw resourceObject: " + rObj.toXml());
            Rule customizationRule = appObject.getCustomizationRule();
            if (null != customizationRule) {
                log.debug("Customization rule found for applicaiton " + applicationName);
                // Pass the mandatory arguments to the Customization rule for the app.
                HashMap ruleArgs = new HashMap();
                ruleArgs.put("context", context);
                ruleArgs.put("log", log);
                ruleArgs.put("object", rObj);
                ruleArgs.put("application", appObject);
                ruleArgs.put("connector", appConnector);
                ruleArgs.put("state", new HashMap());

                // Call the customization rule just like a normal aggregation would.
                ResourceObject newRObj = (ResourceObject) context.runRule(customizationRule, ruleArgs, null);

                // Make sure we got a valid resourceObject back from the rule.
                if (null != newRObj) {
                    rObj = newRObj;
                    log.debug("Got post-customization resourceObject: " + rObj.toXml());
                }
            }

// Next we perform a miniature "Aggregation" using IIQ's built in Aggregator.
// Create an arguments map for the aggregation task.
// To change this (if you need to), the map contains aggregation options and is the same as the
// arguments to the acocunt aggregation tasks.  Some suggestied defaults are:
            Attributes argMap = new Attributes();
            argMap.put("promoteAttributes", "true");
            argMap.put("correlateEntitlements", "true");
            argMap.put("noOptimizeReaggregation", "true");  // Note: Set to false to disable re-correlation.
            Aggregator agg = new Aggregator(context, argMap);
            if (null == agg) {
                errorMessage = "Null Aggregator returned from constructor.  Unable to Aggregate!";
                log.error(errorMessage);
                return errorMessage;
            }
            log.debug("Calling aggregate() method... ");
            TaskResult taskResult = agg.aggregate(appObject, rObj);
            log.debug("aggregation complete.");

            if (null == taskResult) {
                errorMessage = "ERROR: Null taskResult returned from aggregate() call.";
                log.error(errorMessage);
                return errorMessage;
            }
            log.debug("TaskResult details: \n" + taskResult.toXml());
            return ("Success");
        } catch (sailpoint.connector.ObjectNotFoundException onfe) {
            errorMessage = "Connector could not find account: [" + accountName + "]";
            errorMessage += " in application  [" + applicationName + "]";
            log.error(errorMessage);
            log.error(onfe);
            return errorMessage;
        } catch (GeneralException | ConnectorException e) {
            e.printStackTrace();
        }
        return "Success";
    }
}
