package sailpoint.plugin.identitysearchplugin.rest;

import sailpoint.api.ManagedAttributer;
import sailpoint.api.SailPointContext;
import sailpoint.api.SailPointFactory;
import sailpoint.object.Application;
import sailpoint.object.ManagedAttribute;
import sailpoint.tools.GeneralException;

import java.util.*;

public class Test {
    SailPointContext context;
    private static final String value="CN=AnyAdmin,OU=Heirarchy,OU=Groups,OU=Demo,DC=seri,DC=sailpointdemo,DC=com";
    public List getHeirarchy(String value, String appName){
        List<List<String>> masterList=new ArrayList();
            try {
                context = SailPointFactory.getCurrentContext();
                Application application=context.getObject(Application.class,appName);
                ManagedAttribute ma= ManagedAttributer.get(context,application.getId(),"memberof",value);
                if(null!=ma){
                    //Starting with finding the first level parent groups of current ma.
                    List<ManagedAttribute> firstLevelAttrb=ma.getInheritance();
                    //Building a ArrayDeque to insert and remove elements at the iteration level.
                    Deque<ManagedAttribute> groups=new ArrayDeque(firstLevelAttrb);
                    while(!groups.isEmpty()){
                        ManagedAttribute tempAttrb= (ManagedAttribute) groups.pollFirst();
                        List<String> tempNode=new ArrayList<>();
                        tempNode.add(tempAttrb.getDisplayableName());
                        tempNode.add(tempAttrb.getDisplayableName());
                        tempNode.add(tempAttrb.getDescription("en_us"));
                        for(ManagedAttribute childNode:tempAttrb.getInheritance()) {
                            groups.addLast(childNode);
                        }
                        masterList.add(tempNode);
                    }

//                    if(null!=firstLevelAttrb&&!firstLevelAttrb.isEmpty()){
//                        ListIterator iterator=firstLevelAttrb.listIterator();
//                        while(iterator.hasNext()){
//
//                        }
//                    }
                }
            } catch (GeneralException e) {
                e.printStackTrace();

        }
        return masterList;
    }


}
