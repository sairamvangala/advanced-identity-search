package sailpoint.plugin.identitysearchplugin.rest;

import java.util.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import sailpoint.api.SailPointContext;
import sailpoint.api.SailPointFactory;
import sailpoint.api.TaskManager;
import sailpoint.object.*;
import sailpoint.rest.plugin.BasePluginResource;
import sailpoint.rest.plugin.RequiredRight;
import sailpoint.tools.GeneralException;

@RequiredRight(value = "advanceIdentityRESTAllow")
@Path("advancedEdit")
public class AdvanceIdentityResource extends BasePluginResource {
	public static final Log log = LogFactory.getLog(AdvanceIdentityResource.class);

	@GET
	@Path("getIdentityNames")
	@Produces("application/json")
	public List<String> getIdentityNames(){
		List<String> returnNames=new ArrayList<>();
		QueryOptions queryOptions=new QueryOptions();
		SailPointContext context= null;
		try {
			context = SailPointFactory.getCurrentContext();
			Iterator iterator= context.search(Identity.class,queryOptions,"name");
			while(iterator.hasNext()){
				Object [] thisRecord = (Object[]) iterator.next();
				String name=(String) thisRecord[0];
				returnNames.add(name);
			}
		} catch (GeneralException e) {
			log.error("Error occured in context retreival");
		}
		return returnNames;
	}
	@GET
	@Path("getNamesStartWith/{searchString}")
	@Produces("application/json")
	public List<String> getMatchingNames(@PathParam("searchString") String searchString){
		List<String> matchingNames=new ArrayList<>();
		if(null==searchString){
			log.debug("searchString is so calling other method to return all values");
			return getIdentityNames();
		}
		Filter nameFilter=Filter.like("name",searchString, Filter.MatchMode.ANYWHERE);
		QueryOptions queryOptions=new QueryOptions();
		queryOptions.add(nameFilter);
		SailPointContext context= null;
		try {
			context = SailPointFactory.getCurrentContext();
			Iterator iterator=context.search(Identity.class,queryOptions,"name");
			while(iterator.hasNext()){
				Object [] thisRecord = (Object[]) iterator.next();
				String name=(String) thisRecord[0];
				matchingNames.add(name);
			}
		}catch (Exception e){
			log.error("Error occured in searching, with searchString:"+e);
		}
		return matchingNames;
	}
	@GET
	@Path("getAttributeNames")
	@Produces("application/json")
	public Map getAttrbNames(){
		Map<String,String> attribMap=new HashMap<>();
		Locale enLocal=new Locale("en");
		SailPointContext context=null;
		try {
			context=getContext();
			ObjectConfig objectConfig = context.getObjectByName(ObjectConfig.class,"Identity");
			List<ObjectAttribute> baseList=objectConfig.getObjectAttributes();
			for(ObjectAttribute objectAttribute:baseList){
				attribMap.put(objectAttribute.getName(),objectAttribute.getDisplayableName(enLocal));
			}
		}catch (Exception e){
			log.error("Error occurred in attribute name retrieval"+e);
		}
		return attribMap;
	}
	@GET
	@Path("getIdentityAttrbs/{name}")
	@Produces("application/json")
	public static Map getIdentityAttributes(@PathParam("name") String name){
		System.out.println("searching for name:"+name);
		Map identityAttrb=new HashMap();
		SailPointContext context=null;
		try {
			context=SailPointFactory.getCurrentContext();
			Identity identity = context.getObjectByName(Identity.class, name);
			System.out.println("Got the IDentity as:"+identity);
			identityAttrb=identity.getAttributes().getMap();
			identityAttrb.put("ID",identity.getId());
			System.out.println("returning attributes:"+identityAttrb);
			return identityAttrb;
		}catch (Exception e){
			log.error("Error occurred in Identity Attribute Retrieval method:"+e);
		}
		return identityAttrb;
	}

	@POST
	@Path("refreshIdentity")
	@Consumes("application/json")
	@Produces("application/json")
	public static Map doRefresh(Map optionMap){
		log.error("Starting the refresh method with body:"+optionMap);
		Map refreshResponse=new HashMap();
		SailPointContext context=null;
		try {
			context=SailPointFactory.getCurrentContext();
			TaskManager taskManager=new TaskManager(context);
			TaskResult refResult=taskManager.runSync("Refresh Identity Cube",optionMap);
			refreshResponse=refResult.getAttributes().getMap();
			log.error("Refresh Result:"+refreshResponse);
		}catch (Exception e){
			log.error("Error occured in refresh method with error:"+e);
			refreshResponse.put("error",e.toString());
		}
		return refreshResponse;
	}
	@POST
	@Path("getAppLinks/{idName}")
	@Produces("application/json")
	@Consumes("application/json")
	public static Map<String,String> getAppLinks(@PathParam("idName") String idName){
		log.trace("Starting the method getAppLinks");
		Map<String,String> appNames=new HashMap<>();
		if(null!=idName){
			SailPointContext context=null;
			try {
				context=SailPointFactory.getCurrentContext();
				Identity identity=context.getObjectByName(Identity.class,idName);
				if(null!=identity){
					List<Link> links=identity.getLinks();
					for(Link link:links){
						if(null!=link&&null!=link.getApplicationName()){
							appNames.put(link.getApplicationName(),link.getNativeIdentity());
						}
					}
					log.debug("Build links names are:"+appNames);
					return appNames;
				}else{
					log.error("No matching Identity Found");
				}
			}catch (Exception e){
				log.error("Got error in the getApplinks method:"+e);
			}
		}else{
			log.error("got the idName as null");
		}
		log.trace("Exiting the method getAppLinks");
		return appNames;
	}
	@POST
	@Path("doSingleAggr")
	@Produces("application/json")
	@Consumes("application/json")
	public static Map doSingleAggr(Map acctSelection){
		Map appResults=new HashMap();

		log.trace("Starting the method doSingleAggr:"+acctSelection);
		SingleAccountAggregation accountAggregation=new SingleAccountAggregation();
		for (Object key:acctSelection.keySet()){
			String keyVal= (String) key;
			String tempRes=accountAggregation.doSingleAggt(keyVal, (String) acctSelection.get(keyVal));
			appResults.put(keyVal,tempRes);
		}
		log.error("Exiting the method doSingleAggr:");
		return appResults;
	}
	@Override
	public String getPluginName() {
		return "identitySearchPlugin";
	}
}
